import React, {useEffect, useState} from 'react';
function SalespeopleList(){
    const [salespeopleList, setSalespeopleList] = useState([])
    async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok){
            const data = await response.json();
            setSalespeopleList(data.salespeople)
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadSalespeople();
    }, []);
    return (
        <div>
        <h1 className="fw-bold">Salespeople</h1>
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {salespeopleList.map(salesperson => {
                    return (
                        <tr key={salesperson.employee_id}>
                            <td>{salesperson.first_name} {salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </div>
    );
}
export default SalespeopleList;
