import React, {useEffect, useState} from 'react';

function AutomobilesList() {

  const [automobiles, setAutomobiles] = useState([]);

  const loadAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.status === 200) {
      const data = await response.json();
      setAutomobiles(data.autos);
    };
  };

  useEffect(() => {
    loadAutomobiles();
  }, []);

  return (
    <div className="px-4 py-5 my-5 text-left">
      <h1 className="display-5 fw-bold">Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody className="text-capitalize">
          {automobiles.map(auto => {
            return (
              <tr key={auto.vin}>
                <td>{auto.vin}</td>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.model.name}</td>
                <td>{auto.model.manufacturer.name}</td>
                <td>{auto.sold.toString()}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );

}

export default AutomobilesList;
