import React, {useEffect, useState} from 'react';
function SalesHistory() {
    const [sales, setSales] = useState([]);
    const loadSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales)
    }
    else {
        console.error(response)
    }
    }
    const [salespeople, setSalespeople] = useState([]);
    const loadSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople)
    }
    else {
        console.error(response)
    }
    }

    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    useEffect(() => {
        loadSales();
        loadSalespeople();
    }, []);
    return (
            <div className="my-2">
            <h1 className="fw-bold">Sales History</h1>
            <div>
            <div className="select p-4 mt-4">
                <select onChange={handleSalespersonChange} value={salesperson} required type="text" id="salesperson-select" name="salesperson"  placeholder="Salesperson" className="form-select">
                <option value=''>Choose Salesperson</option>
                {salespeople.map(salesperson => {
                return (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                );
            })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>

                {sales.filter(sale =>
            sale.salesperson.employee_id === salesperson).map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        </div>


        )
            };

export default SalesHistory;
