import React, { useState } from "react";

function CreateManufacturerForm() {
  const [name, setManufacturerName] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();
    const data = { name };

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";

    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      setManufacturerName("");
    }
  }

  function handleChangeManufacturer(event) {
    const value = event.target.value;
    setManufacturerName(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" value={name} name="name" id="name" className="form-control" />
              <label htmlFor="name">Manufacturer...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateManufacturerForm;
