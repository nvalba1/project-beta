import React, {useState} from 'react';

function CreateSalesperson() {
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const [employeeId, setEmployeeId] = useState('');
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;


        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok){



            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    };
    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
            <input onChange={handleFirstNameChange} placeholder="Firstname" required type="text" value={firstName} name="firstname" id="firstname" className="form-control"/>
                <label htmlFor="firstname">First Name</label>
            </div>
            <div className="form-floating mb-3">
            <input onChange={handleLastNameChange} placeholder="Lastname" required type="text" value={lastName} name="lastname" id="lastname" className="form-control"/>
                <label htmlFor="lastname">Last Name</label>
            </div>
            <div className="form-floating mb-3">
            <input onChange={handleEmployeeIdChange} placeholder="employeeid" required type="text" value={employeeId} name="employeeid" id="employeeid" className="form-control"/>
                <label htmlFor="employeeid">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
        </div>
        );
}

export default CreateSalesperson;
