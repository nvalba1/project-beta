from django.urls import path
from .views import api_customers, api_delete_customer, api_delete_salesperson, api_salespeople, api_sales, api_delete_sale

urlpatterns = [
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:id>/", api_delete_customer, name="api_delete_customer"),
    path("salespeople/", api_salespeople, name="api_salespeople"),
    path("salespeople/<int:id>/", api_delete_salesperson, name="api_delete_salesperson"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:id>/", api_delete_sale, name="api_delete_sale"),
]
