from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Sale, Salesperson, Customer, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "first_name", "last_name", "address", "phone_number"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerListEncoder(),
        "salesperson": SalespersonListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        response = JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
            safe = False,
        )
        response.status_code = 200
        return response
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            response = JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,

                )
            response.status_code = 201
            return response
        except:
            response = JsonResponse(
                {"message": "Cannot create salesperson"},
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            response = JsonResponse(
                salesperson,
                encoder = SalespersonListEncoder,
                safe = False,
            )
            response.status_code = 200
            return response
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_customers(request):
        if request.method == "GET":
                customers = Customer.objects.all()
                response = JsonResponse(
                    {"customers": customers},
                    encoder=CustomerListEncoder,
                    safe=False,
                )
                response.status_code = 200
                return response
        else:
            try:
                content = json.loads(request.body)
                customer = Customer.objects.create(**content)
                response = JsonResponse(
                    customer,
                    encoder=CustomerListEncoder,
                    safe=False,
                )
                response.status_code = 201
                return response
            except:
                response = JsonResponse(
                    {"message": "Cannot create customer"}
                )
                response.status_code = 400
                return response

@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            response = JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False
            )
            response.status_code = 200
            return response
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        response = JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False
        )
        response.status_code = 200
        return response
    else:
        content = json.loads(request.body)

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin, sold=False)
            content["automobile"] = automobile

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            automobile.sold = True
            automobile.save()

            sale = Sale.objects.create(**content)
            response = JsonResponse(
                    sale,
                    encoder=SaleListEncoder,
                    safe=False
                )
            response.status_code = 201
            return response
        except:
            response = JsonResponse({
                "message": "cannot create sale"
                })
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
        try:

            sale = Sale.objects.get(id=id)
            sale.delete()

            sale.automobile.sold = False
            sale.automobile.save()
            response = JsonResponse(
                sale,
                encoder=SaleListEncoder,
                safe=False
            )
            response.status_code = 200
            return response
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale does not exist"}
            )
            response.status_code = 400
            return response
